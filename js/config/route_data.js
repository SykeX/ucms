var route_data = {
    _: [
        {
            route: '/home',
            homePage: true,
            template: 'views/home-template.php',
            needLogin: false,
        },
        {
            route: '/404',
            template: 'views/404.html',
            needLogin: false,
        },
        {
            route: '/contact',
            template: 'views/contact.html',
            needLogin: false,
        },
        {
            route: '/news',
            template: 'views/news/news-template.html',
            needLogin: false,
            data: [
                {add_insert: ['add_stuff', 'blog_tags', ['post_id', 'tag'], false, false]},
                {get_blog_comments_count: []},
                {add_blog_posts_modifier: ['likes', '+', ['likeCallback', 'db', 'column', 'id', 'operator', 'user_id']]},
                {set_filter: ['blog_posts', 3, 1]},
                {add_blog_posts_watcher: []},
                {add_blog_posts_updater: []}
            ]
        },
        {
            route: '/news/:child',
            template: 'views/news/post-template.html',
            needLogin: false,
            data: [
                {bindTo_blog_posts: []},

                {add_blog_posts_modifier: ['likes', '+', ['likeCallback', 'db', 'column', 'id', 'operator', 'user_id']]},
                {add_upload: ['upl', 'media/img/']},

                {deleteFrom_blog_comments: ['child_blog_comments', ['deletedComment', 'obj', 'user_id']]},
                {deleteFrom_blog_posts: [undefined, ['deletedPost', 'obj', 'user_id']]},

                {get_parent_relatives: ['tags', 'parent_rels', 'blog_posts']},
                {set_object: ['DSykesDidDat', [1, 2, 3, 4]]},
                {set_child_blog_comments_filter: [4, 1]},

                {get_blog_comments_insertEditor: ['comField', ['parent_id', 'createdAt', 'comment', 'user_id', 'likes'], ['commentAdded', 'db', 'obj', 'user_id']]},
                {add_blog_posts_updater: []},
            ],
            callback: 'postCallback'
        },
        {
            route: '/edit_post',
            template: 'views/news/edit_post.html',
            needLogin: true,
            accessLevel: ['a', 'md'],
            data: [
                {add_upload: ['uploadThumb', 'media/img/', 'thumb']},
                {get_blog_posts_insertEditor: ['postBodyField', ['title', 'picture', 'body', 'user_id', 'excerpt', 'likes', 'tags', 'category', 'createdAt'], ['postAdded', 'db', 'obj', 'user_id']]}
            ]
        },
        {
            route: '/edit_post/:child',
            template: 'views/news/edit_post.html',
            needLogin: true,
            accessLevel: '*',
            hasData: true,
            data: [
                {get_blog_posts: ['parent']},
                {add_upload: ['uploadThumb', 'media/img/', 'thumb']},
                {set_specials: ['default']},
                {get_blog_posts_updateEditor: ['postBodyField', ['title', 'picture', 'body', 'user_id', 'excerpt', 'category'], ['postUpdated', 'obj', 'user_id']]},
            ]
        },
        {
            route: '/login',
            template: 'views/login-template.html',
            needLogin: false,
            accessLevel: '*',
        },
        {
            route: '/register',
            template: 'views/signup-template.html',
            needLogin: false,
            accessLevel: '*',
            data: [
                {add_upload: ['uploadPicture', 'media/img/prof_pics/', 'thumb']}
            ]
        },
        {
            route: '/user/:child',
            template: 'views/membership/profile.html',
            needLogin: false,
            data: [
                {bindTo: ['users']},
                {get_profiles: ['userProfile', 'user_id', 'parent', false]},
                {get_blog_posts: ['userPosts', 'user_id', 'parent', true]},
                {get_blog_comments: ['userComments', 'user_id', 'parent', true]},
                //{get_blog_comments_by: ['parent_id', 'userPosts', 'id', 'userComments']},
                {get_forum_topics: ['userTopics', 'user_id', 'parent', true]},
                {get_forum_comments_by: ['parent_id', 'userTopics', 'id', 'userTopicComments']},
                //{join_objects: ['userComments', 'userTopicComments', 'userActivity']},
                {set_filter: ['userPosts', 3, 1]},
                {set_filter: ['userComments', 3, 1]},
                {set_filter: ['userTopicComments', 3, 1]},
                {set_filter: ['userTopics', 3, 1]},
               //{add_specific_watcher: ['userComments', 'blog_comments']},
                //{get_location: ['location']},
                {add_users_updater: []}
            ]
        },
        {
            route: '/manage_account/:child',
            template: 'views/membership/edit_account.html',
            needLogin: true,
            accessLevel: '*',
            callback: 'manageAccountCallback',
            data: [
                {get_users: ['parent']},
                {set_specials: ['default']},
                {updateFor_users: [['display_name', 'password', 'picture'], ['uUpdated', 'db', 'obj', 'id', 'user_id']]},
                {add_upload: ['uploadPicture', 'media/img/prof_pics/', 'thumb']}
            ]
        },
        {
            route: '/manage_profile/:child',
            template: 'views/membership/edit_profile.html',
            needLogin: true,
            accessLevel: '*',
            callback: 'manageProfileCallback',
            data: [
                {bindTo_profiles: []},
                {updateFor_profiles: [['bio', 'interests'], ['pUpdated', 'obj']]},
            ]
        },
        {
            route: '/create_profile/:child',
            template: 'views/membership/create_profile.html',
            needLogin: true,
            accessLevel: '*',
            callback: 'createProfileCallback',
            data: [
                {parent_db: ['users']},
                {insertFor_profiles: [['bio', 'interests', 'user_id'],['profileCreated', 'obj']]}
            ]
        },
        {
            route: '/forums',
            template: 'views/forums/boards.html',
            needLogin: false,
            data: [
                {deleteFrom_forum_boards: [undefined, ['deletedBoard', 'obj', 'user_id']]},
                {add_forum_boards_watcher: []},
                {add_forum_boards_updater: []}
            ]
        },
        {
            route: '/board/:child',
            template: 'views/forums/board.html',
            needLogin: false,
            callback: 'forumBoardCallback',
            data: [
                {bindTo_forum_boards: []},
                {deleteFrom_forum_boards: [undefined, ['deletedBoard', 'obj', 'user_id']]},
                {set_filter: ['child_forum_topics', 5, 1]}
            ]
        },
        {
            route: '/topic/:child',
            template: 'views/forums/topic.html',
            needLogin: false,
            data: [
                {bindTo: ['forum_topics']},
                //{get_object: ['children', 'get_replies', 'replies', 'forum_comments', 'parent_id']},
                {get_forum_comments_insertEditor: ['replyField', ['parent_id', 'comment', 'user_id', 'createdAt'], ['replyAdded', 'db', 'obj', 'user_id']]},
                {set_filter: ['child_forum_comments', 5,1]},
                {deleteFrom_forum_comments: ['child_forum_comments', ['deletedTopicComment', 'obj', 'user_id']]},
                {deleteFrom_forum_topics: [undefined, ['deletedTopic', 'obj', 'user_id']]},
                {add_forum_topics_updater: []},
            ]
        },
        {
            route: '/edit_topic/:child',
            template: 'views/forums/edit_topic.html',
            needLogin: true,
            accessLevel: '*',
            callback: 'editTopicCallback',
            data: [
                {get_forum_topics: ['parent', 'forum_boards']},
                {set_specials: ['default']},
                {get_forum_topics_by: ['parent_id', 'parentObj', 'id', 'topics']},
                {set_filter: ['topics', 5, 1]},
                {get_forum_topics_updateEditor: ['topicBodyField', ['title', 'body', 'createdAt'], ['topicUpdated', 'db', 'obj', 'user_id', 'scope', 'resp']]}
            ],

        },
        {
            route: '/create_board',
            template: 'views/forums/create_board.html',
            needLogin: true,
            accessLevel: ['a', 'md'],
            callback: 'createBoardCallback',
            data:[
                {insertFor_forum_boards: [['title', 'description'], ['boardCreated', 'obj']]}
            ]

        },
        {
            route: '/edit_board/:child',
            template: 'views/forums/edit_board.html',
            needLogin: true,
            accessLevel: ['a', 'md'],
            callback: 'editBoardCallback',
            data: [
                {bindTo_forum_boards: []},
                {updateFor_forum_boards: [['title', 'description'], ['boardUpdated', 'obj']]}
            ]
        },
        {
            route: '/create_topic/:child',
            template: 'views/forums/edit_topic.html',
            needLogin: true,
            accessLevel: '*',
            callback: 'createTopicCallback',
            data: [
                {parent_db: ['forum_boards']},
                {get_forum_topics: ['topics', 'parent_id', 'parent', true]},
                {set_filter: ['topics', 5, 1]},
                {get_forum_topics_insertEditor: ['topicBodyField', ['parent_id', 'title', 'body', 'user_id', 'createdAt'], ['topicAdded', 'db', 'obj', 'user_id', 'scope']]}
            ],

        }
    ]
};