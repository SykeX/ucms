
/*----------------------------------------------*/
/* Main Configurations for the site goes here   */
/*----------------------------------------------*/

var Configs = {

    siteTitle: 'UnoCMS Demo',

    BACKEND_URL: 'https://dsykes.firebaseio.com/',

    partialDir: 'views/partials/',

    controller: 'Ctrl',

    /*-----   Database Configs  -----*/
    /*-------------------------------*/
    //db_connection: '',
    db_host: '127.0.0.1',
    db_name: 'mydb',
    db_username: 'root',
    db_password: 'maxwel123',

    db: ['users', 'blog_posts', 'blog_comments', 'profiles', 'forum_boards', 'forum_topics', 'forum_comments'],
    dbRelations: [
        {blog_posts: ['blog_comments', 'parent_id']},
        {forum_boards: ['forum_topics', 'parent_id']},
        {forum_topics: ['forum_comments', 'parent_id']},
        {users: ['blog_posts', 'user_id']},
        {users: ['forum_topics', 'user_id']},
        {users: ['blog_comments', 'user_id']},
        {users: ['forum_comments', 'user_id']},
        {users: ['profiles', 'user_id']},
        {blog_posts: ['post_likes', 'post_id']}
    ],

    editorCols: ['comment', 'body', 'hero'],

    modRanks: ['a', 'md'],

    strictForGuests: true,

    strictLevel: 'heavy',

    global_data: [
        {set_object: ['testGlobal', 'KOBE!!']},
    ],

    notInUse: {
        // Configure db load based on routes
    },

    textEditor:''
};